#!/usr/bin/python
'''This is a CGI script that displays the registration ID for the 
Android device in order to ensure proper receipt from the device'''

import cgi
import shelve
import cgitb
cgitb.enable()

#This opens the configuration file
config = shelve.open('/opt/app/UserConfig')
print 'Content-Type: text/html'
print
print '<title>Reg ID Check</title>'
print '<h1>Your Registration ID</h1>'
print 'Your ID is: '
#The registration ID is retrieved from the configuration file
print str(config['regID'])
config.close()
