#!/usr/bin/python
'''This is a CGI script that allows the Android application to make
and HTTP POST request that stores the registration ID in a permanent
configuration file for later retrieval'''

import cgi
import shelve

#This opens the configuration file
storage = shelve.open('/opt/app/UserConfig')
#This retrieves the fields from an HTML form
form = cgi.FieldStorage()
#Storing the registration ID from the HTML form into the permanent configuration file
storage['regID']=form.getfirst("regid")
storage.close()
print "Content-Type: text/html"
print
print "<title>User Config</title>"
print "<H1>Configuration Complete</H1>"
print "You have successfully configured your system"
