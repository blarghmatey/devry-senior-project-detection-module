!#/usr/bin/bash
#This script downloads and installs all of the necessary software for the
#detection algorithms to run. 

sudo apt-get -y purge ffmpeg x264 libx264-dev
sudo apt-get -y install gcc gfortran python python-dev python-setuptools python-scipy python-numpy build-essential checkinstall git cmake libfaac-dev libjack-jackd2-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libsdl1.2-dev libtheora-dev libva-dev libvdpau-dev libvorbis-dev libx11-dev libxfixes-dev libxvidcore-dev texi2html yasm zlib1g-dev python-imaging python-pygame libgstreamer0.10-0 libgstreamer0.10-dev gstreamer0.10-tools gstreamer0.10-plugins-base libgstreamer-plugins-base0.10-dev gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly gstreamer0.10-plugins-bad gstreamer0.10-ffmpeg subversion libgtk2.0-0 libgtk2.0-dev libjpeg62 libjpeg62-dev

sudo easy_install simplecv

cd ~/Downloads
wget ftp://ftp.videolan.org/pub/videolan/x264/snapshots/x264-snapshot-20120327-2245-stable.tar.bz2
tar -xvjf x264-snapshot-20120327-2245-stable.tar.bz2
cd x264-snapshot-20120327-2245-stable
./configure --enable-static --enable-shared --enable-pic
make
sudo make install

cd ~/Downloads
wget http://ffmpeg.org/releases/ffmpeg-0.8.10.tar.bz2
tar -xvjf ffmpeg-0.8.10.tar.bz2
cd ffmpeg-0.8.10
./configure --enable-pic --enable-shared --enable-gpl --enable-libfaac \
--enable-libmp3lame --enable-libopencore-amrnb --enable-libopencore-amrwb \
--enable-libtheora --enable-libvorbis --enable-libx264 --enable-libxvid \
--enable-nonfree --enable-postproc --enable-version3 --enable-x11grab
make
sudo make install

cd ~/Downloads
wget http://www.linuxtv.org/downloads/v4l-utils/v4l-utils-0.8.6.tar.bz2
tar -xvjf v4l-utils-0.8.6.tar.bz2
cd v4l-utils-0.8.6
make
sudo make install

cd ~/Downloads
wget http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/2.3.1/OpenCV-2.3.1a.tar.bz2
tar -xvjf OpenCV-2.3.1a.tar.bz2
cd OpenCV-2.3.1a
mkdir build
cd build
cmake ../ -D:CMAKE_BUILD_TYPE=RELEASE -D:BUILD_PYTHON_SUPPORT=ON ..
make
sudo make install
