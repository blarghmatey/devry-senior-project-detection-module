''' This file is a script that allows the user to view the operation
of the detection algorithms. It opens a window that displays the feed
from the IP camera and when a face or body is detected it draws a randomly
colored box around the region where the object was detectd. '''

import SimpleCV
import random
import shelve
from Detect_Class import DetectionModule

'''This block instantiates an instance of the DetectionModule class and 
sets some local variables for use within the main body of the code'''
modInstance = DetectionModule('UserConfig')
storage = shelve.open('UserConfig')
username = storage['user']
password = storage['pass']
threshold = int(storage['threshold'])
detectUrl = 'http://' + storage['url'] + '/snapshot.cgi?user=' + username + '&pwd=' + password
''' This is an image that is used to compare subsequent frames against
to determine whether there has been a significant change in the environment'''
baseImage = SimpleCV.ImageClass.Image(detectUrl)

while True:
	#An image is retrieved from the IP camera
    img = SimpleCV.ImageClass.Image(detectUrl)
    haarPath = '/usr/local/share/OpenCV/haarcascades/'
    #These statements check for the presence of the objects defined in the respective cascade files
    face = img.findHaarFeatures(haarPath + "haarcascade_frontalface_alt2.xml")
    face2 = img.findHaarFeatures(haarPath + "haarcascade_frontalface_alt.xml")
    face3 = img.findHaarFeatures(haarPath + "haarcascade_frontalface_default.xml")
    face4 = img.findHaarFeatures(haarPath + "haarcascade_profileface.xml")
    body1 = img.findHaarFeatures(haarPath + "haarcascade_fullbody.xml")
    body2 = img.findHaarFeatures(haarPath + "haarcascade_upperbody.xml")
    body3 = img.findHaarFeatures(haarPath + "haarcascade_lowerbody.xml")
    #The program first checks to determine if there has been a change in the scenery
    if(modInstance.imageTest(baseImage, img, threshold)):
		#If a sufficient difference in scenery is found the program tests for the presence of a face or body
        if(face and face2):
			#This line draws a randomly colored box around the area of detection
            face.draw((random.randint(0,255),random.randint(0,255),random.randint(0,255)))
        if(face and face3):
            face.draw((random.randint(0,255),random.randint(0,255),random.randint(0,255)))
        if(face2 and face3):
            face2.draw((random.randint(0,255),random.randint(0,255),random.randint(0,255)))
        if(body1 and body2):
            body1.draw((random.randint(0,255),random.randint(0,255),random.randint(0,255)))
            body2.draw((random.randint(0,255),random.randint(0,255),random.randint(0,255)))
    #This opens a window that displays the retrieved image, along with any drawn boxes
    img.show()
