#!/bin/bash
#This is a script for installing an apache module that allows for HTTP
#streaming of H264 encoded MP4 files. It also configures Apache
#with a virtual host file for being able to access the web page from
#a remote browser

sudo apt-get -y install apache2 apache2-threaded-dev

cd ~/Downloads
wget http://h264.code-shop.com/download/apache_mod_h264_streaming-2.2.7.tar.gz
tar -xvzf apache_mod_h264_streaming-2.2.7.tar.gz
cd mod_h264_streaming-2.2.7
./configure --with-apxs=`which apxs2`
sudo make
sudo make install

mkdir /tmp/recordings

sudo cp /etc/apache2/apache2.conf /etc/apache2/apache2.conf.original
sudo chown $USER:$USER /etc/apache2/apache2.conf
echo '
LoadModule h264_streaming_module /usr/lib/apache2/modules/mod_h264_streaming.so
AddHandler h264-streaming.extensions .mp4' >> /etc/apache2/apache2.conf
sudo chown root:root /etc/apache2/apache2.conf

echo '<VirtualHost *:80>
        ServerName blarghmatey.chickenkiller.com
	ServerAlias *
        DocumentRoot /tmp/recordings
        <Directory /tmp/recordings>
                AllowOverride all
                Order allow,deny
                Allow from all
        </Directory>
</VirtualHost>' >> /home/$USER/StoredStream
sudo mv /home/$USER/StoredStream /etc/apache2/sites-available
sudo ln -s /etc/apache2/sites-available/StoredStream /etc/apache2/sites-enabled/StoredStream

sudo cp /etc/apache2/sites-available/default /etc/apache2/sites-available/default.original
sudo chown $USER:$USER /etc/apache2/sites-available/default
sed -i 's/\/var\/www\//\/tmp\/recordings\//g' /etc/apache2/sites-available/default
sudo chown root:root /etc/apache2/sites-available/default

sudo chown -R root:root /etc/apache2/
sudo /etc/init.d/apache2 restart
