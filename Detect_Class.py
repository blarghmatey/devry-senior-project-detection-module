'''This is the class definition for the detection algorithm. It also includes
the methods for generating and sending alerts, as well as recording the
video after an alert is generated.'''

from SimpleCV import ImageClass
import urllib
import urllib2
import datetime
import subprocess
import re
import shelve
import numpy

class DetectionModule:
    
    #Initialize all of the class variables for use in other methods
    def __init__(self, shelfFile='UserConfig'):
        self.shelfFile = shelfFile
        #Open the configuration file
        storage = shelve.open(self.shelfFile)
        username = storage['user']
        password = storage['pass']
        #Set the threshold for activity levels before detection algorithms are run
        self.threshold = int(storage['threshold'])
        self.detectUrl = 'http://' + storage['url'] + '/snapshot.cgi?user=' + username + '&pwd=' + password
        self.recordUrl = 'http://' + storage['url'] + '/videostream.cgi?user=' + username + '&pwd=' + password
        self.haarPath = '/usr/local/share/OpenCV/haarcascades/'
        self.img = None
        self.imgName = None
        self.vidFile = None
        self.alertTime = None
        self.recordTime = storage['record']
        self.baseImage = ImageClass.Image(self.detectUrl)
        storage.close()

    def faceDetect(self):
        #This method will test an image for the presence of a face
        img = ImageClass.Image(self.detectUrl)
        face = img.findHaarFeatures(self.haarPath + "haarcascade_frontalface_alt2.xml",1.1, 5, 1)
        face2 = img.findHaarFeatures(self.haarPath + "haarcascade_frontalface_alt.xml",1.1, 5, 1)
        face3 = img.findHaarFeatures(self.haarPath + "haarcascade_frontalface_default.xml",1.1, 5, 1)
        #Only run the face detection if the activity threshold has been exceeded
        if self.imageTest(self.baseImage, img, self.threshold):
            if(((face and face2) or (face and face3) or (face2 and face3))):
                return True
        
        
    def bodyDetect(self):
        #This method will test an image for the presence of a body
        img = ImageClass.Image(self.detectUrl)
        body1 = img.findHaarFeatures(self.haarPath + "haarcascade_fullbody.xml",1.1, 5, 1)
        body2 = img.findHaarFeatures(self.haarPath + "haarcascade_upperbody.xml",1.1, 5, 1)
        body3 = img.findHaarFeatures(self.haarPath + "haarcascade_lowerbody.xml",1.1, 5, 1)
        #Only run the face detection if the activity threshold has been exceeded
        if self.imageTest(self.baseImage, img, self.threshold):
            if((body1 and body2)):
                return True
        
    def raiseAlert(self):
        #This method sets all of the variables relevant to the generated alert
        self.alertTime = datetime.datetime.now()
        self.imgName = self.alertTime.strftime("%m-%d-%y-%H%M%S") + '-alert.jpg'
        self.vidFile = self.alertTime.strftime("%m-%d-%y-%H%M%S") + '-alert.mp4'
        self.img = ImageClass.Image(self.detectUrl)
        self.img.scale(0.5).save('/opt/recordings/' + self.imgName)
        #Call the method to send the alert to the Android device
        self.sendAlert()
        #Call the method to record footage from the IP camera
        self.vidRecord()
        
    def vidRecord(self):
		#This method records the feed from the IP camera for a predetermined length
        startTime = datetime.datetime.now()
        #Send a command to the Bash shell to run the ffmpeg binary for transcoding the video
        bashCmd = 'ffmpeg -r 15 -f mjpeg -s vga -i \'' + str(self.recordUrl) + '\' -t ' + str(self.recordTime) + ' -vcodec libx264 -vpre medium ' + '/opt/recordings/' + str(self.vidFile)
        subprocess.call(bashCmd, shell=True)
        
    def sendAlert(self):
		#This method sends the generated alert to the Android application via the C2DM framework
        alert = urllib2.Request('https://android.apis.google.com/c2dm/send')
        storage = shelve.open(self.shelfFile)
        alertData = {'collapse_key':'someString','data.payload':self.imgName,'data.vidFile':self.vidFile, 
                    'data.alertTime':self.alertTime.strftime("%m/%d/%y-%H:%M:%S"), 'registration_id':storage['regID']}
        postData = urllib.urlencode(alertData)
        alert.add_data(postData)
        alert.add_header('Authorization', 'GoogleLogin auth=' + storage['c2dmAuth'])
        result = urllib2.urlopen(alert)

    def imageTest(self, img1=None, img2=None, tolerance=None):
		#This method tests to determine whether the frame that is being analyzed is different than a certain baseline image
		#This information is used to determine whether there has been a change in scenery, such as a person or vehicle
		#coming into the field of view
        if(img1.width == img2.width and img1.height == img2.height):
            diff = (img2-img1)
            value = numpy.average(diff.getNumpy())
            print value
            if value > tolerance:
                return True
            else:
                return False
