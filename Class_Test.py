'''This file imports and executes the methods defined in the DetectionModule
class'''

import Detect_Class
import datetime
from SimpleCV import ImageClass
import time

#Instantiate an instance of the DetectionModule class
testing = Detect_Class.DetectionModule('UserConfig')

print 'The detection algorithm is now running'
start = datetime.datetime.now()

while(True):
	#Periodically refresh the baseline image for determining whether the scenery has changed
    if datetime.datetime.now() - start > datetime.timedelta(minutes=5):
        start = datetime.datetime.now()
        testing.baseImage = ImageClass.Image(testing.detectUrl)
    #Run the face and body detection algorithms. If either of them returns true then send an alert
    if(testing.faceDetect() or testing.bodyDetect()):
        if(testing.faceDetect() or testing.bodyDetect()):
            testing.raiseAlert()
            #Pause the detection for a period so continual alerts are not sent while someone is present
            time.sleep(60)
