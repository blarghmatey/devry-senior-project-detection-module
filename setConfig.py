#!/usr/bin/python
'''This is a CGI script that allows the user to set up the detection
system to communicate with the IP camera, as well as determining some
operational settings. It also causes the server to register with the 
Android Cloud to Device Messaging Service in order to send alerts to
the Android application'''

import cgi
import shelve
import urllib, urllib2
import re

#Open the configuration file for storing permanent data
storage = shelve.open('/opt/app/UserConfig')
#Retrieve the values from the HTML form that called the script
form = cgi.FieldStorage()
#Assign the HTML form fields to the proper dictionary key for permanent storage
storage['url'] = form.getfirst("camUrl")
storage['user'] = form.getfirst("user")
storage['pass'] = form.getfirst("pass")
storage['record'] = form.getfirst("recordTime")
storage['threshold'] = form.getfirst("threshold")

#Send an HTTP post request to Google for registering the server with the
#C2DM service and store the authorization key in permanent storage
authData = {'Email':'DeVry.Android@gmail.com','Passwd':'teamandroid','accountType':'GOOGLE',
			'source':'TeamAndroid-DetectAlert-v0.1','service':'ac2dm'}
postData = urllib.urlencode(authData)
getAuth = urllib2.Request('https://www.google.com/accounts/ClientLogin')
getAuth.add_header('Content-Type','application/x-www-form-urlencoded')
getAuth.add_data(postData)
response = urllib2.urlopen(getAuth)
authToken = re.sub('.*\n.*\nAuth=', '', response.read())
storage['c2dmAuth'] = re.sub('\n','',authToken)

storage.close()
print "Content-Type: text/html"
print
print "<title>User Config</title>"
print "<H1>Configuration Complete</H1>"
print "You have successfully configured your system"
